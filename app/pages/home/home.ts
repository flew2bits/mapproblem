import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {MapPage} from '../map/map';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController) {

  }

  navigateToMapWithPush() {
    this.navCtrl.push(MapPage);
  }

  navigateToMapWithSetRoot() {
    this.navCtrl.setRoot(MapPage);
  }
}
