import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GoogleMap, GoogleMapsEvent, GoogleMapsMarker, GoogleMapsMarkerOptions, GoogleMapsLatLng } from 'ionic-native'
import { HomePage } from '../home/home';


/*
  Generated class for the MapPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/map/map.html',
})
export class MapPage {
  map: GoogleMap;
  constructor(private navCtrl: NavController) {

  }

  ionViewDidEnter() {
    this.map = new GoogleMap('gmap');
    GoogleMap.isAvailable().then(() => {
    })
  }

  close() {
    if (this.navCtrl.canGoBack()) this.navCtrl.pop();
    else this.navCtrl.setRoot(HomePage);
  }

}
